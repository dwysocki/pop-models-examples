# Pop Models Examples

The [Pop Models](https://git.ligo.org/daniel.wysocki/bayesian-parametric-population-models) population inference package can be used to estimate the properties of populations, given a set of noisy and selection-biased detections from that population.  While easily applied to other problems, this package is primarily used to infer the merger rate, mass distribution, and spin distribution of compact binary objects detected in gravitational waves from instruments like LIGO and Virgo.

This repository demonstrates the usage of this package, through an ever-growing set of example analyses.


## Examples

1. Open Data Analysis
    - [Mass and spin distribution constraints from LIGO/VIRGO GWTC-1 open data](runs/End-of-O2-Mass-and-Spin-Analysis)

2. Mock Data Analysis
    - **Coming soon:** Toy Models
    - **Coming soon:** Real models with synthetic PE

3. Supporting Data Products
    - [Computing sensitive volumes](runs/vts)
        - [Assuming zero spin](runs/vts/zero-spin-efficient-grid)
        - **Coming soon:** Assuming aligned spins

4. Code Tests
    - **Coming soon:** Percentile-Percentile tests
    - **Coming soon:** Population distribution function validity checks


## Maintainers

Daniel Wysocki and Richard O'Shaughnessy