m_min=5.0
m_max=195.0
M_max=200.0

log_M_samples_coarse=20
qtilde_samples_coarse=11

log_M_samples_fine=50
qtilde_samples_fine=20

waveform_fmin=19.0

vt_threads=48
