#!/bin/bash

source filenames.sh
source vt_settings.sh

repo/bin/python plot_component_mass.py \
    $vt_coarse_file $plot_component_mass_coarse &

repo/bin/python plot_component_mass.py \
    $vt_fine_file $plot_component_mass_fine &

for job in $(jobs -p)
do
    wait $job
done
