#!/bin/bash

source filenames.sh
source vt_settings.sh

seed=1

repo/bin/python repo/bin/pop_models_vt \
    $m_min $m_max 1.0 $vt_coarse_file \
    --total-mass-max $M_max \
    --log-total-mass-samples $log_M_samples_coarse \
    --mass-ratio-samples $qtilde_samples_coarse \
    --zero-spin \
    --f-min $waveform_fmin \
    --n-threads $vt_threads \
    --seed $seed &

repo/bin/python repo/bin/pop_models_vt \
    $m_min $m_max 1.0 $vt_fine_file \
    --total-mass-max $M_max \
    --log-total-mass-samples $log_M_samples_fine \
    --mass-ratio-samples $qtilde_samples_fine \
    --zero-spin \
    --f-min $waveform_fmin \
    --n-threads $vt_threads \
    --seed $seed &

for job in $(jobs -p)
do
    wait $job
done
