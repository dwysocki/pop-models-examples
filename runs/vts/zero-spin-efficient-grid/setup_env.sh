# Undo change directory, as sourcing `lalinference_o2.sh` takes control of the
# shell and stops the top-level setup script from moving back to the OLDPWD.
cd ../../..

# Load LALInference O2.
# Note: This will load an environment with LALInference O2 on the LIGO clusters,
# but on a non-LIGO machine will likely fail.  If you are running from a
# non-LIGO machine, you will need to install LALInference, specifically the
# lalinference_o2 snapshot.  It is available on GitHub here:
# https://github.com/lscsoft/lalsuite/tree/lalinference_o2/lalinference
source ~cbc/pe/lalinference_o2.sh
