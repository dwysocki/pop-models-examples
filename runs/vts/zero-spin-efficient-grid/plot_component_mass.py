def get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument("vts")
    parser.add_argument("plot_filename")

    parser.add_argument(
        "--n-samples",
        type=int, default=500,
        help="Number of samples to use in plotting.",
    )
    parser.add_argument(
        "--n-contours",
        type=int, default=100,
        help="Number of contours to show in plot.",
    )
    parser.add_argument(
        "--cmap",
        default="viridis",
        help="Colormap to use.",
    )

    return parser.parse_args(raw_args)


def main(raw_args):
    args = get_args(raw_args)

    import h5py
    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt

    from pop_models import vt

    with h5py.File(args.vts, "r") as vt_file:
        m_min = vt_file.attrs["m_min"]
        m_max = vt_file.attrs["m_max"]
        M_max = vt_file.attrs["M_max"]

        mass = numpy.linspace(m_min, m_max, args.n_samples)
        M1, M2 = numpy.meshgrid(mass, mass, indexing="ij")
        m1, m2 = M1.ravel(), M2.ravel()

        VT = (
            vt.RegularGridVTInterpolator(vt_file)(m1, m2)
        ).reshape(M1.shape)

        # Convert so T = 1 in the units used, and we can just treat this as V
        V = VT * 365.25


    fig, ax = plt.subplots()

    ctr = ax.contourf(
        M1, M2, numpy.log10(V),
        args.n_contours,
        cmap=args.cmap,
    )

    cbar = fig.colorbar(ctr, ax=ax)

    ax.set_xlabel(r"$m_1^{\mathrm{source}} / \mathrm{M}_\odot$")
    ax.set_ylabel(r"$m_2^{\mathrm{source}} / \mathrm{M}_\odot$")
    cbar.set_label(r"$\log_{10} V / \mathrm{Gpc}^3$")

    fig.savefig(args.plot_filename)


if __name__ == "__main__":
    import sys
    raw_args = sys.argv[1:]
    sys.exit(main(raw_args))
