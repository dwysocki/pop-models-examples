#!/bin/bash

source data.sh
source filenames.sh

# Install corner.py if it isn't already.
repo/bin/pip install corner

# Plot the events.
repo/bin/python plot_events.py \
  $events \
  results/events_m.png results/events_a.png \
  results/events_tilt.png results/events_chieff_q.png &

# Run the population inference MCMC.
./mcmc.sh

# From the raw MCMC samples, compute things like burnin and auto-correlation to
# get the subset of the MCMC samples which most accurately represent the
# posterior distribution.
./extract_samples.sh

# Make a corner plot of the posteriors.
repo/bin/python corner_plot.py \
  "${post_cleaned_file}" \
  "results/corner.png" \
  "results/corner_mass.png" "results/corner_spin.png"
