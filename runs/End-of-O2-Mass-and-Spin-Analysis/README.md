# Mass and spin distribution constraints from LIGO/VIRGO GWTC-1 open data

Here we make joint inferences on the BBH event rate as well as the BBH mass and spin distributions, using the ten BBH detections in the GWTC-1 parameter estimation data release ([link](https://dcc.ligo.org/LIGO-P1800370/public)).

*Note: The current results are preliminary, as our MCMC chains have not had time to converge.  Production-scale chains take about 1 week to run, and the data have only been public for a few days.  This will be updated when production results are complete.*


## Model overview

**Coming soon**


## Predicted population distributions

**Coming soon**


## Posterior distributions on population hyperparameters

To see a corner plot of *all* hyperparameter posteriors, [click here](results/corner.png).

### Mass distribution hyperparameters

![mass parameters](results/corner_mass.png)

### Spin distribution hyperparameters

![spin parameters](results/corner_spin.png)


## Sampling diagnostics

[Click here](DIAGNOSTICS.md) to view a set of MCMC sampling diagnostics.