#!/usr/bin/env python
from __future__ import print_function

import astropy.units
import astropy.cosmology
import numpy
import os
import re
import h5py

source_pe_dir = "files/GWTC-1_sample_release/"
pe_sample_file_pattern = "^GW[0-9]{6}_GWTC-1.hdf5$"

def dist2z(luminosity_distance):
    z = numpy.empty_like(luminosity_distance)
    for idx, dL in numpy.ndenumerate(luminosity_distance):
        z[idx] = astropy.cosmology.z_at_value(
            astropy.cosmology.Planck15.luminosity_distance,
            dL*astropy.units.Mpc,
            zmin=0.001, zmax=2.0,
        )
    return z

for basename in os.listdir(source_pe_dir):
    fname = os.path.join(source_pe_dir, basename)

    # Skip files that aren't posterior sample files.
    if not re.search(pe_sample_file_pattern, basename):
        continue
    # Skip GW170817 since we don't model BNS's here.
    if re.search("170817", basename):
        continue

    basename_txt = basename.split(".")[0] + ".txt"
    output_fname = os.path.join("files", "event_posteriors", basename_txt)

    print("Converting posteriors for", basename[:8])
    with h5py.File(fname, "r") as h5_file:
        posteriors_source = h5_file["SEOBNRv3_posterior"]
        m1_det = posteriors_source["m1_detector_frame_Msun"]
        m2_det = posteriors_source["m2_detector_frame_Msun"]
        a1 = posteriors_source["spin1"]
        a2 = posteriors_source["spin2"]
        costilt1 = posteriors_source["costilt1"]
        costilt2 = posteriors_source["costilt2"]
        dL = posteriors_source["luminosity_distance_Mpc"]

        one_plus_z = 1.0 + dist2z(dL)

        m1_source = m1_det / one_plus_z
        m2_source = m2_det / one_plus_z

        M = m1_source + m2_source
        q = m2_det / m1_det
        chi_eff = (m1_source*a1*costilt1 + m2_source*a2*costilt2) / M

        output_data = numpy.column_stack((
            m1_source, m2_source,
            a1, a2, costilt1, costilt2,
            M, q, chi_eff,
        ))
        numpy.savetxt(
            output_fname,
            output_data,
            header=" ".join([
                "m1_source", "m2_source",
                "a1", "a2", "costilt1", "costilt2",
                "mtotal_source", "q", "chi_eff",
            ]),
        )
