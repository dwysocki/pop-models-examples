mcmc_seed=4

mcmc_samples_per_walker=3000
mcmc_walkers=200

mcmc_fixed_burnin=1500
mcmc_fixed_acorr=100

mcmc_skip_first_n=100
