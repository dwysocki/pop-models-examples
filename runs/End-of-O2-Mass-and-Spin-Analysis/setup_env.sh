#!/bin/bash

# Ensure directory to hold output data products exists.
mkdir -p results/

# Ensure directory to hold data files exists.
mkdir -p files/

# Enter data directory.
cd files

# Fetch VT calibration factors.
wget https://dcc.ligo.org/public/0156/T1800427/002/calibration_quadratic.json

# Fetch event posterior samples.
wget https://dcc.ligo.org/public/0157/P1800370/001/GWTC-1_sample_release.tar.gz
tar xvzf GWTC-1_sample_release.tar.gz
rm GWTC-1_sample_release.tar.gz

# Move out of data directory as we now need to run a script in the parent
# directory.
cd ..

# Convert event posterior samples to text format used by PopModels
mkdir files/event_posteriors
./convert_event_posteriors.py
