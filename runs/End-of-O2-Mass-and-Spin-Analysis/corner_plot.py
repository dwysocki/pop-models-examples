from __future__ import division, print_function


all_params = [
    "log10_rate",
    "alpha_m", "beta_q", "m_min", "m_max",
    "E_chi1", "Var_chi1", "E_chi2", "Var_chi2",
    "mu_cos1", "sigma_cos1", "mu_cos2", "sigma_cos2",
]

mass_params = [
    "log10_rate",
    "alpha_m", "beta_q", "m_min", "m_max",
]

spin_params = [
    "log10_rate",
    "E_chi1", "Var_chi1", "E_chi2", "Var_chi2",
    "mu_cos1", "sigma_cos1", "mu_cos2", "sigma_cos2",
]

param_groups = {
    "all" : all_params,
    "mass" : mass_params,
    "spin" : spin_params,
}

def get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument("posteriors")
    parser.add_argument("output_plot_full")
    parser.add_argument("output_plot_massonly")
    parser.add_argument("output_plot_spinonly")

    parser.add_argument("--truths", nargs="+", type=float)

    return parser.parse_args(raw_args)


def plot(filename, data, labels, truths):
    import corner
    import matplotlib.pyplot as plt

    fig = corner.corner(
        data,
        labels=labels, truths=truths,
        levels=[0.50, 0.90],
        hist_kwargs={
            "linewidth": 2.5, "density": True,
        },
        contour_kwargs={
            "linestyles": "solid", "linewidths": [2, 1],
        },
        no_fill_contours=True, plot_datapoints=True, plot_density=False,
        data_kwargs={
            "color": "#ff7f0e",
        },
    )
    fig.savefig(filename)
    plt.close(fig)
    


def main(raw_args):
    args = get_args(raw_args)

    import six

    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import h5py

    from pop_models.powerlaw_spin_vec import plot_config

    plot_groups = {
        "all" : args.output_plot_full,
        "mass" : args.output_plot_massonly,
        "spin" : args.output_plot_spinonly,
    }

    # Load posterior samples.
    with h5py.File(args.posteriors, "r") as posteriors_file:
        data = posteriors_file["pos"].value
        variable_names = posteriors_file.attrs["variable_names"].split(",")

    for group_name in six.iterkeys(plot_groups):
        # Determine the filename to save plot to.
        plot_fname = plot_groups[group_name]
        # Determine the names of all parameters we can plot for this group.
        param_names = [
            name for name in param_groups[group_name]
            if name in variable_names
        ]
        # Determine which column each parameter lies in.
        param_indices = [
            variable_names.index(name)
            for name in param_names
        ]
        # Extract the columns that correspond to the desired parameters.
        group_data = data[..., param_indices]
        # Determine the labels for each parameter.
        param_labels = []
        for name in param_names:
            label = plot_config.labels.get(name, None)
            units = plot_config.units.get(name, None)
            if label is None:
                param_labels.append(name)
            elif units is None:
                param_labels.append("${}$".format(label))
            else:
                param_labels.append("${} / {}$".format(label, units))

        # Extract the appropriate 'truths' from the CLI input if given.
        if args.truths is None:
            truths = None
        else:
            truths = [args.truths[i] for i in param_indices]

        plot(plot_fname, group_data, param_labels, truths)


if __name__ == "__main__":
    import sys
    raw_args = sys.argv[1:]
    sys.exit(main(raw_args))
