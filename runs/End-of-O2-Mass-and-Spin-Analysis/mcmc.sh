#!/bin/bash

source data.sh
source filenames.sh
source mcmc_settings.sh

# Set the maximum total mass above 2*m_max so it has no effect.
Mmax=200.0


repo/bin/python repo/bin/pop_models_powerlaw_spin_vec_mcmc \
    ${events} "${vt_file}" "prior.json" "${post_file}" \
    --constants "constants.json" \
    --total-mass-max "${Mmax}" \
    --vt-calibration "${calibration_file}" \
    --no-spin-singularities \
    --n-samples "${mcmc_samples_per_walker}" --n-walkers "${mcmc_walkers}" \
    --n-threads 1 \
    --seed "${mcmc_seed}" \
    --verbose
