from __future__ import division, print_function

colors = [
    "#1f77b4", "#ff7f0e",
    "#2ca02c", "#d62728",
    "#9467bd", "#8c564b",
    "#e377c2", "#7f7f7f",
    "#bcbd22", "#17becf"
]


def get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "event_posteriors",
        nargs="+",
        help="Posterior samples for each event.",
    )

    parser.add_argument(
        "output_plot_m",
        help="File to save component mass plot to.",
    )

    parser.add_argument(
        "output_plot_a",
        help="File to save component spin magnitude plot to.",
    )

    parser.add_argument(
        "output_plot_tilt",
        help="File to save component spin tilt plot to.",
    )

    parser.add_argument(
        "output_plot_chieff_q",
        help="File to save component chi_eff, q plot to.",
    )

    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib.",
    )

    return parser.parse_args(raw_args)



def main(raw_args):
    args = get_args(raw_args)

    import glob
    import os.path
    import itertools
    color_cycle = itertools.cycle(colors)

    import numpy
    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt

    # Set up figure for plotting.
    fig_m, ax_m = plt.subplots(figsize=[6,4])
    fig_a, ax_a = plt.subplots(figsize=[6,4])
    fig_tilt, ax_tilt = plt.subplots(figsize=[6,4])
    fig_chieff_q, ax_chieff_q = plt.subplots(figsize=[6,4])

    # List of posterior files
    posterior_filenames = args.event_posteriors
    

    # Loop over each event and plot its posteriors and true values.
    for i, (color, filename) in enumerate(zip(color_cycle, posterior_filenames)):
        # Load posterior samples.
        posterior_samples = numpy.genfromtxt(filename, names=True)

        # Plot masses
        plot_post(
            ax_m,
            posterior_samples["m1_source"], posterior_samples["m2_source"],
            color,
        )
            
        # Plot spin magnitudes
        plot_post(
            ax_a,
            posterior_samples["a1"], posterior_samples["a2"],
            color,
        )


        # Plot spin tilts
        plot_post(
            ax_tilt,
            posterior_samples["costilt1"], posterior_samples["costilt2"],
            color,
        )


        # Plot chieff vs q
        plot_post(
            ax_chieff_q,
            posterior_samples["q"], posterior_samples["chi_eff"],
            color,
        )


    # Format and save figures.

    ax_m.set_xlabel(r"$m_1^{\mathrm{source}}$ [M$_\odot$]")
    ax_m.set_ylabel(r"$m_2^{\mathrm{source}}$ [M$_\odot$]")
    fig_m.tight_layout()
    fig_m.savefig(args.output_plot_m)

    ax_a.set_xlabel(r"$\chi_1$")
    ax_a.set_ylabel(r"$\chi_2$")
    fig_a.tight_layout()
    fig_a.savefig(args.output_plot_a)

    ax_tilt.set_xlabel(r"$\cos\theta_1$")
    ax_tilt.set_ylabel(r"$\cos\theta_2$")
    fig_tilt.tight_layout()
    fig_tilt.savefig(args.output_plot_tilt)

    ax_chieff_q.set_xlabel(r"$q$")
    ax_chieff_q.set_ylabel(r"$\chi_{\mathrm{eff}}$")
    fig_chieff_q.tight_layout()
    fig_chieff_q.savefig(args.output_plot_chieff_q)


def plot_post(ax, x, y, color):
    ax.scatter(x, y, color=color, marker="o", s=2, zorder=0)


if __name__ == "__main__":
    import sys
    raw_args = sys.argv[1:]
    sys.exit(main(raw_args))
