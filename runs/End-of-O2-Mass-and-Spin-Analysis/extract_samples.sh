#!/bin/bash

source filenames.sh
source mcmc_settings.sh

## TEMPORARY: for runs that are not production scale in sample size,
## burnin and auto-correlation will be problematic, as there will not be enough
## samples to fully exclude burnin or accurately compute the auto-correlation
## time.  So we hard-code the number of samples to discard for burnin and
## auto-correlation in order to ensure we have a non-zero sample size, accepting
## that the samples will not be fully burned in or un-correlated.
##
## If production-scale results are done, one can comment-out everything
## preceeding the `exit` statement, and proper burnin/acorr analysis will be
## done.
repo/bin/python repo/bin/pop_models_extract_samples \
  "${post_file}" "${post_cleaned_file}" \
  --fixed-auto-correlation "$mcmc_fixed_acorr" \
  --fixed-burnin "$mcmc_fixed_burnin" \
  --full-chain-plot "${full_chain_plot}" \
  --cleaned-chain-plot "${cleaned_chain_plot}" \
  --skip-first-n-in-plotting "${mcmc_skip_first_n}"

exit

repo/bin/python repo/bin/pop_models_extract_samples \
  "results/posteriors.hdf5" "results/posteriors_cleaned.hdf5" \
  --force \
  --max-posterior-burnin \
  --integrated-auto-correlation m=5 \
  --full-chain-plot "${full_chain_plot}" \
  --cleaned-chain-plot "${cleaned_chain_plot}" \
  --max-posterior-burnin-plot "${burnin_plot}" \
  --integrated-auto-correlation-plot "${acorr_plot}" \
  --skip-first-n-in-plotting "${mcmc_skip_first_n}"
