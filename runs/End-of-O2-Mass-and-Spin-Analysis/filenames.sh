# File tabulating VT(m1, m2) on a grid.
vt_file="../vts/zero-spin-efficient-grid/results/vt_m1_m2_fine.hdf5"
# File containing calibration coefficients for VT.
calibration_file="files/calibration_quadratic.json"

# File containing raw MCMC samples
post_file=results/posteriors.hdf5
# File containing extracted MCMC samples
post_cleaned_file=results/posteriors_cleaned.hdf5
# Files plotting MCMC diagnostics
full_chain_plot=results/chain_full.png
cleaned_chain_plot=results/chain_cleaned.png
burnin_plot=results/burnin.png
acorr_plot=results/acorr.png
