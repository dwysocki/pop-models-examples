#!/bin/bash

RUNDIR=$1
PREVPWD="${PWD}"

echo entering run dir
cd "${RUNDIR}"

echo sourcing setup script
source setup_env.sh

cd "${PREVPWD}"
