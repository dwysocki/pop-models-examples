#!/bin/bash

RUNDIR=$1

# Path to file with commit's SHA
commit_sha_path="$RUNDIR/commit.sha"
# Read the sha from the file
read -r commit_sha < $commit_sha_path

# Url to download repository from
git_url="git@git.ligo.org:daniel.wysocki/bayesian-parametric-population-models.git"

# Directory to download repostiory into
repo_dir="$RUNDIR/repo/"

# If directory exists, simply checkout the desired commit after syncing
if [ -d "$repo_dir" ]
then
    # Enter the repository's directory
    cd $repo_dir

    # Pull any new changes
    git pull

    # Switch to the commit corresponding to the desired SHA
    git reset --hard $commit_sha
# If directory doesn't exist: clone, checkout, and setup the install
else
    # Clone the repository
    git clone $git_url $repo_dir
    # Enter the newly cloned repository's directory
    cd $repo_dir
    # Switch to the commit corresponding to the desired SHA
    git reset --hard $commit_sha

    # Initialize and activate a virtual environment to install within
    virtualenv .
    source bin/activate

    # Update pip
    python bin/pip install -U pip

    # Install the library
    python bin/pip install -Ue .

    # Deactivate virtual environment
    deactivate
fi
