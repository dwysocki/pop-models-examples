#!/bin/bash

RUNDIR=$1

SPECDIR="$RUNDIR/specs"
FILEDIR="$RUNDIR/files"

checksum_log="$RUNDIR/checksum.log"
status=0

for specpath in "$RUNDIR/specs"/*.spec
do
    # Get just the name of the spec file, without the leading directory.
    spec=$(basename $specpath)
    # Get the filename associated with that specfile -- remove .spec extension
    file=${spec%.spec}
    # Get the path to the associated file
    filepath="$FILEDIR/$file"

    # Read the first line from the spec file -- should be an md5sum
    read -r spec_md5 < $specpath
    # Compute the md5sum of the file
    file_md5=$(md5sum $filepath | cut -c 1-32)

    # Check if the sums match
    if [ "$spec_md5" = "$file_md5" ]
    then
        # Sums matched! This file is good.
        echo \
            "File '${file}' passes. md5: ${file_md5}" \
            >> $checksum_log
    else
        # Sums did not match. Set exit status to failure, and log error.
        status=1
        echo \
            "File '${file}' fails. md5: ${file_md5}, should be ${spec_md5}" \
            >> $checksum_log
    fi
done

if [[ "$status" -ne "0" ]]
then
    >&2 echo "Some files did not match"
fi

exit $status
