#!/bin/bash

RUNDIR=$1

# Enter directory
cd $RUNDIR

# Delete the repo
rm -rf "repo"

# Run cleanup script
./cleanup.sh
