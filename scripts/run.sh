#!/bin/bash

RUNDIR=$1

# Enter virtualenv
echo entering venv
source $RUNDIR/repo/bin/activate

# Run script
echo entering run dir
cd $RUNDIR
echo executing run.sh
./run.sh

# Exit virtualenv
echo exiting venv
deactivate
